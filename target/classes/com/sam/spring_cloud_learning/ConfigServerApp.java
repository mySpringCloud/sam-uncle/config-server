package com.sam.spring_cloud_learning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * @EnableConfigServer
 * 
 * 开启Spring Cloud Config 的服务端功能
 *
 */
@SpringBootApplication
@EnableConfigServer
public class ConfigServerApp {
	/**
	 * 启动并验证
　　　　访问配置信息的URL与配置文件的映射关系如下：
		/{application}/{profile} [/{label}]
		/{application}-{profile}.yml
		/{label}/{application}-{profile}.yml
		/{application}-{profile}.properties
		/{label}/{appliction}-{profile}.properties
		　　　　上面的url会映射{application}-{profile}.properties对应的配置文件，其中{label}对应Git上不同的分支，默认是master。

　　　　通过浏览器访问http://localhost:7001/sam/dev/my_test,结果如下：
		{
			"name": "sam",
			"profiles": ["dev"],
			"label": "my_test",
			"version": "3d37a8ed08a697421dc682e8d4800dfd4bd265d7",
			"state": null,
			"propertySources": [{
				"name": "https://gitlab.com/mySpringCloud/sam-uncle/spring-cloud-learning.git/spring-cloud-config-file/sam-dev.properties",
				"source": {
					"from": "my_test_git-dev-1.0"
				}
			},
			{
				"name": "https://gitlab.com/mySpringCloud/sam-uncle/spring-cloud-learning.git/spring-cloud-config-file/sam.properties",
				"source": {
					"from": "my_test_git-1.0"
				}
			}]
		}
		注意：上面的分支名字千万不能保护-横杠，比如：config-label-test
	 * @param args
	 */
    public static void main(String[] args) {
        SpringApplication.run(ConfigServerApp.class, args);
    }
}
